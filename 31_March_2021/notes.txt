Embed youtube video in your HTML Page 

- Go to https://www.youtube.com/
- Search for video 
- Select video 
- Click on Share> Embed
- Copy Iframe tag and paste in your HTML Page 
(Adjust height/width accordingly)